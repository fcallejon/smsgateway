var modem = require('modem').Modem();
var prompt = require('prompt');

var properties = [
    {
        name: 'message',
        validator: /^[a-zA-Z\s\-]+$/,
        warning: 'Message must be only letters, spaces, or dashes'
    },
    {
        name: 'number',
        validator: /^[0-9]+$/,
        warning: 'Only numbers PLEASE!!'
    }
];

prompt.start();

prompt.get(properties, function (err, result) {
    if (err) { return onErr(err); }
    result.number = '+' + result.number;
    console.log('Command-line input received:');
    console.log('  message: ' + result.message);
    console.log('  number: ' + result.number);
    
    modem.open('/dev/ttyUSB3', function () {
        modem.execute('AT+CMGF=1', function (escapeChar, response) {
            
            console.log('PDU');
            console.log('escape_char: ' + escapeChar);
            console.log('response: ' + response);
            console.log('');
            
            modem.execute('AT+CSCS="GSM"', function (escapeChar, response) {
            
            console.log('GSM');
            console.log('escape_char: ' + escapeChar);
            console.log('response: ' + response);
            console.log('');

            modem.execute('AT+CMGS="' + result.number + '"', function(escapeChar, response) {
                
                console.log('NUMBER');
                console.log('escape_char: ' + escapeChar);
                console.log('response: ' + response);
                console.log('');

                modem.execute(result.message, function (escapeChar, response) {
                    
                    console.log('MESSAGE');
                    console.log('escape_char: ' + escapeChar);
                    console.log('response: ' + response);
                    console.log('');

                }, 0, 60);

            }, 0, 60);
            
            /*
            console.log('lalala');
            modem.on('delivery', function (details, index) {
                console.log('Deleting message');
                modem.deleteMessage(index);
                console.log('Index: ' + index);
                console.log('Details: ');
                console.log(details);
            });
            
            var message = {
                text: result.message,
                receiver: result.number,
                encoding: '16bit'
            };
            
            modem.sms(message, function (err, references) {
                console.log('error: ' + err);
                console.log('ref: ' + references);
            });
            */
            }, 0, 60);
        }, 0, 60);

    });
    
    return true;
});

function onErr(err) {
    console.log(err);
    return 1;
}