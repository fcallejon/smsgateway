import fs 			from 'fs';
import chokidar 	from 'chokidar';
import shortid 		from 'shortid';

class SmsClient{
	constructor() {
		const filePath = '/var/spool/sms/outgoing/';
		const sentPath = '/var/spool/sms/sent/';
		const failedPath = '/var/spool/sms/failed/';
	}

	sendSms(number, message, callback) {
		var fileName = shortid.generate();
		var fullFile = filePath + fileName;
		var fileContent = 'To: ' + number + '\r\n\r\n' + message;

		var sent = chokidar.watch(sentPath + fileName,
			{ ignored: /[\/\\]\./, persistent: true });
		var failed = chokidar.watch(failedPath + fileName,
			{ ignored: /[\/\\]\./, persistent: true });

		fs.writeFile(fullFile, fileContent, 'utf8', (err) => {
			if (err) {
				throw err;
			}

			console.log('It\'s saved! ' + fullFile);

			sent.on('all', function (event, path) {
				if (event !== 'add') {
					return true;
				}

				console.log('message sent. ' + path);
				if (failed != null) {
					failed.close();
				}
				callback(true);
				return true;
			});

			failed.on('all', function (event, path) {
				if (event !== 'add') {
					return true;
				}

				console.log('message failed. ' + path);
				if (sent != null) {
					sent.close();
				}
				callback(false);
				return true;
			});
		});
	};
};
export default SmsClient;
