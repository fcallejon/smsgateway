var smsClient = smsClient();
router.route('/sms/')

    // create a bear (accessed at POST http://localhost:ROME/api/sms)
    .post(function(req, res) {

        if (!req.body.number){
            res.status(400).send('You must include a valid mobile number.');
            return;
        }
        if (!req.body.text){
            res.status(400).send('You must include a valid text to be sent.');
            return;
        }
        smsClient.sendSms(req.body.number, req.body.text)
    })

    // get all the bears (accessed at GET http://localhost:ROME/api/sms)
    .get(function(req, res) {

    });
