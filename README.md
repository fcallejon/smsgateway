# Messagy SMS Gateway #

### How do I get set up? ###

* clone
* npm install
* npm run start-dev

### To add a cron at startup ###

```
#!bash
sudo crontab -u root -e
```
* Add the following
```
@reboot [PATH-TO-CODE]/node_modules/.bin/forever start --append -o [PATH-TO-CODE]/gateway.log -e [PATH-TO-CODE]/gateway.log [PATH-TO-CODE]/node_modules/.bin/babel-node --presets es2015 server.js
```
```
#!bash
sudo crontab -u root -l
```
```
#!bash
sudo shutdown -r now
```
enjoy!