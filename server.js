import restify from 'restify';
import WifiConfigurator from './wifi.js';
import smsroutes from './sms-routes.js';
import log4js from 'log4js';
import config from 'config';
import bleno from 'bleno';
import WifiService from './wifi-service';
const server                = restify.createServer();
const wificonf              = new WifiConfigurator();
const wifiservice	    = new WifiService(wificonf);
const shutdownOnAppClosing  = config.get('wifi.shutdownOnAppClosing')

// User hit Ctrl + C
var killingApp = false;
process.on('SIGINT', function () {
    console.log('\n');

    console.log('[PROGRESS] Gracefully shutting down from SIGINT (Ctrl+C)');
    try {
        wificonf.shutdown();
    } catch (error) {
        console.log(error);
    }
    
    process.exit();
});

var port = process.env.PORT || 7663;
server.use((req, res, next) => {
    console.log('We need to add validation here.');
    next();
});

server.pre(restify.pre.userAgentConnection());
//server.use(restify.acceptParser(server.acceptable));
//server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.gzipResponse());
server.use(restify.bodyParser());
//server.use(restify.requestExpiry());

// test route to make sure everything is working
// (accessed at GET http://localhost:7663/api)
server.get('/', (req, res) => {
    res.json({ message: 'WHAAAAAAAAT! oh yeah it DOES works.' });
});
new smsroutes(server, '/api/');

server.listen(port, () => {
  console.log('%s listening at %s', server.name, server.url);
});


wificonf.on('connected', (network) => {
    console.log(network);
});

wificonf.on('disconnected', (reason) => {
    console.log(reason);
});

bleno.on('stateChange', function(state) {
  console.log('on -> stateChange: ' + state);

  if (state === 'poweredOn') {
    bleno.startAdvertising('wifi', [wifiservice.uuid]);
  } else {
    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', function(error) {
  console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));
  
  if (!error) {
    bleno.setServices([
      wifiservice
    ]);
  }
});
