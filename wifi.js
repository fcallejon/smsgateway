"use strict";

import cp from 'child_process';
import config from 'config';
import events from 'events';
import log4js from 'log4js';
import fs from 'fs';
//TODO: Move to config file
log4js.configure({
    appenders: [
        {
            type: 'file',
            filename: 'wifi.log',
            category: ['wifi', 'console']
        },
        {
            type: 'console'
        }
    ],
    replaceConsole: true
});
var logger = log4js.getLogger('wifi');
logger.setLevel(config.get('wifi.logLevel') || 'ERROR');

class WifiConfigurator extends events.EventEmitter {
    constructor() {
        super();
        this.network = [];
        this.networks = [];
        this.formerAddress = '';
        this.network = null;

        this.connected = false;
        // The SSID of an open wireless network you'd like to connect to
        this.SSID = config.get('wifi.ssid');
        this.iface = config.get('wifi.iface');
        this.passwd = config.get('wifi.password');
        this.autoConnect = config.get('wifi.autoConnect');
        this.shutdownOnAppClosing = config.get('wifi.shutdownOnAppClosing');
        if (!this.iface) {
            throw new Error("There is no 'iface' configured.");
        }
        if (!this.SSID) {
            throw new Error("There is no 'SSID' configured.");
        }

        this.stopWpaCommand = "service wpa_supplicant stop";
        this.startWpaCommand = "service wpa_supplicant start";
        this.ifDownCommand = "ifdown %IFACE%";
        this.ifUpCommand = "ifup %IFACE%";
        this.removeWpaConfigCommand = "rm -f /etc/wpa_supplicant/wpa_supplicant.conf";

        this.wpaFileModel = `country=GB
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
        ssid="%SSID%"
        psk="%PASSWORD%"
}
`;
    }

    ifIsDown(err, out, errOut, cb){
        if (err) {
            throw new Error(err);
        }
        logger.info("[WIFI] IF is Down");
        cp.exec(
            this.stopWpaCommand,
            (error, stdout, stderr) => this.wpaStopped(error, stdout, stderr, cb));
    }

    wpaStopped(err, out, errOut, cb){
        if (err) {
            throw new Error(err);
        }
        logger.info("[WIFI] WPA Stopped");

        var wpaTemp = this.wpaFileModel.replace("%SSID%", this.SSID);
        wpaTemp = wpaTemp.replace("%PASSWORD%", this.passwd);
        fs.writeFile("/etc/wpa_supplicant/wpa_supplicant.conf", wpaTemp, (err) => this.newWpaFileCreated(err, cb));
    }

    newWpaFileCreated(err, cb){
        if (err) {
            throw new Error(err);
        }
        logger.info("[WIFI] WPA File Created");

        cp.exec(
            this.startWpaCommand,
            (error, stdout, stderr) => this.wpaStarted(error, stdout, stderr, cb));
    }

    wpaStarted(err, out, errOut, cb){
        if (err) {
            throw new Error(err);
        }
        logger.info("[WIFI] WPA Started");

        cp.exec(
            this.ifUpCommand.replace("%IFACE%", this.iface),
            (error, stdout, stderr) => this.ifIsUp(error, stdout, stderr, cb));
    }

    ifIsUp(err, out, errOut, cb){
        if (err) {
            throw new Error(err);
        }
        logger.info("[WIFI] IF it's Up and you should have internet :D");
        if (cb) { cb(); }
    }

    connect(cb) {
        logger.info("[WIFI] Initailizing connection to " + this.SSID);

        cp.exec(
            this.ifDownCommand.replace("%IFACE%", this.iface),
            (error, stdout, stderr) => this.ifIsDown(error, stdout, stderr, cb));
    }

    shutdown(){
//        cp.execSync(this.removeWpaConfigCommand);
//        logger.info("[WIFI] WPA Configuration deleted.");
        cp.execSync(this.stopWpaCommand);
        logger.info("[WIFI] WPA Stopped.");
        cp.execSync(this.ifDownCommand.replace("%IFACE%", this.iface));
        logger.info("[WIFI] IF Down.");
    }
};
export default WifiConfigurator
