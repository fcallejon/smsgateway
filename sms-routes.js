import { default as smsClient } from './SmsClient.js';
class SmsRoutes {
    constructor(server, baseUrl) {
        server.post(baseUrl + '/sms/', (req, res, next) => {
            if (!req.body.number) {
                res.status(400).send(
                    'You must include a valid mobile number.');
                return next();
            }
            if (!req.body.text) {
                res.status(400).send(
                    'You must include a valid text to be sent.');
                return next();
            }
            smsClient.sendSms(req.body.number, req.body.text,
                (result) => {
                    res.writeHead(200,
                        { 'Content-Type': 'application/json; charset=utf-8' });
                    res.end(JSON.stringify({ 'result': JSON.stringify(result) }));
                    //TODO: save result somewhere
                });

            //enable this return when saving message and using queue
            // res.writeHead(200,
            //      { 'Content-Type': 'application/json; charset=utf-8' });
            // res.end(JSON.stringify({ 'messageId': 0 }));
            // return next();
        });
    }
};
export default SmsRoutes;
