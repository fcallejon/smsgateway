var fs = require('fs');
var shortid = require('shortid');
var prompt = require('prompt');

var properties = [
    {
        name: 'message',
        validator: /^[a-zA-Z\s\-]+$/,
        warning: 'Message must be only letters, spaces, or dashes'
    },
    {
        name: 'number',
        validator: /^[0-9]+$/,
        warning: 'Only numbers PLEASE!!'
    }
];

prompt.start();

prompt.get(properties, function (err, result) {
    if (err) { return onErr(err); }
    result.number = '+' + result.number;
    console.log('Command-line input received:');
    console.log('  message: ' + result.message);
    console.log('  number: ' + result.number);
    
    var fileName = shortid.generate();
    var filePath = '/var/spool/sms/outgoing/';
    var sentPath = '/var/spool/sms/sent/';
    var failedPath = '/var/spool/sms/failed/';
    var fullFile = filePath + fileName;
    var fileContent = 'To: ' + result.number + '\r\n\r\n' + result.message;


    var sent = fs.watch(sentPath, {
              ignored: /[\/\\]\./, persistent: true
            });
    var failed = fs.watch(failedPath, {
              ignored: /[\/\\]\./, persistent: true
            });

    fs.writeFile(fullFile, fileContent, 'utf8', (err) => {
        if (err) throw err;

        console.log('It\'s saved! ' + fullFile);

        sent.on('add', function (path)
        {
            console.log('message sent. ' + path);
            console.log(`the current mtime is: ${curr.mtime}`);
            if (failed != null){
                failed.close();
            }
            return true;
        });

        failed.on('add', function (path) {
            console.log('message failed. ' + path);
            console.log(`the current mtime is: ${curr.mtime}`);
            if (sent != null){
                sent.close();
            }
            return true;
        });
    });
    
    return true;
});

function onErr(err) {
    console.log(err);
    return 1;
}